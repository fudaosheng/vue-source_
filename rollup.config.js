import babel from 'rollup-plugin-babel'
import serve from 'rollup-plugin-serve'

export default {
    input:'./src/index.js',//打包入口文件
    output:{//打包出口配置
        file:'dist/umd/vue.js',//出口路径
        name:'Vue',//打包后全局变量名字
        format:'umd',//打包规范
        sourcemap:true,//开启源码调试。babel将高级语法转成低级语法，如：es6->es5，此时需要定位错误在源代码中的位置
    },
    plugins:[//使用插件
        babel({
            exclude:"node_modules/**",//忽略node_modules下的所有文件
        }),
        /**配置serve 
         * process.env.ENV检查环境变量，有'development'时开启服务器，否则不开启
        */
        process.env.ENV === 'development'?serve({
            open:true,
            openPage:'/public/index.html',//服务开启后默认路径
            port:3000,//端口号
            contentBase:''
        }):null
    ]
}