## vue源码笔记

#### Rollup搭建开发环境
1.npm init -y
>初始化package.json文件

2.安装需要的插件
 npm install rollup @babel/core @babel/preset-env rollup-plugin-babel rollup-plugin-serve cross-env -D
 > rollup(打包工具) @babel/core（用babel核心模块） @babel/preset-env（babel将高级语法转成低级语法） rollup-plugin-babel（rollup和babel的桥梁） rollup-plugin-serve（静态服务） cross-env（设置环境变量）