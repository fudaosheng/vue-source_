/**vue工具函数 */

/**判断一个数据是不是对象类型，且不为空 */
export function isObject(data) {
    return typeof data === 'object' && data !== null;
}

/**给一个对象精确的添加或修改属性,即对Object.defineProperty()进一步封装 */
export function def(obj,key,value){
    Object.defineProperty(obj,key,{
        configurable:true,
        enumerable:false,//false不可枚举，不会被映射到for、Object.keys等中
        writable:true,//值是否可改变,
        value
    })
}

export const unicodeRegExp = /a-zA-Z\u00B7\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u037D\u037F-\u1FFF\u200C-\u200D\u203F-\u2040\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD/