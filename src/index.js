/**vue声明、入口 */
import {initMixin} from "./init/index"
function Vue(options){

    /**实际调用初始化方法 */
    this._init(options);
}

initMixin(Vue);

export default Vue