### 使用函数类和class的区别

#### function
```js
import {isObject} from "../utils/tool"

/**对vue中data进行数据劫持 */
export function observe(data){
    /**data不是对象直接返回 */
    if(!isObject(data))return;
    
    new handleObserve(data);
    // handleObserve(data);//函数当构造函数必须要new后才有this，即对于下面this.walk(data);才不会报错
}

/**处理data里面的每一个数据项 */
function handleObserve(data){
    // console.log(this);//直接调用handleObserve时this为undefined，使用new handleObserve(data); 有this

    /**对data进行一层处理 */
    this.walk(data);
}
handleObserve.prototype.walk=function(data){
    const keys = Object.keys(data);

    keys.forEach(key => {
        const value = data[key];
        console.log(value);

        /**核心-响应式处理 */
        defineReactive(data,key,value);
    });
}

function defineReactive(data,key,value){
    Object.defineProperty(data,key,{
        get(){
            return value;
        },
        set(newValue){
            value = newValue;
        }
    })
}
```

#### class
```js
export function observe(data){
    /**data不是对象直接返回 */
    if(!isObject(data))return;
    
    /**处理响应式的class */
    new HandleObserve(data);
}

/**处理data里面的每一个数据项 */
class HandleObserve{
    constructor(data){
        this.walk(data);
    }
    walk(data){
        const keys = Object.keys(data);

        keys.forEach(key => {
            const value = data[key];
    
            /**核心-响应式处理 */
            defineReactive(data,key,value);
        });
    }
}
```