import { initState } from "./initState"
import {compileToFunction} from "../compiler/index"

/**vue初始化入口 */
export function initMixin(vue) {
    /**options new Vue时传进来的选项如props、data、computed、methods、watch等 */
    vue.prototype._init = function (options) {
        const vm = this;//vm就是vue

        /**将new Vue时传入的参数添加到Vue的$options属性上
         * this.$options即指的是用户传递的参数
         */
        vm.$options = options;

        /**初始化状态 */
        initState(vm);



        /**页面挂载与渲染 */
        if (vm.$options.el) {
            this.$mount(vm.$options.el);
        }
    }


    vue.prototype.$mount = function (el) {
        const vm = this;

        const options = vm.$options;
        /**获取el的dom元素 */
        el = document.querySelector(el);

        /**页面渲染顺序:render函数、template模板、html节点 */

        //没有render函数
        if(!options.render){
            let template = options.template;
            /**没有render函数，并且没有template、有el模板 */
            if(!options.template&&options.el){
                template = el.outerHTML;
            }
            console.log(template);
            /**模板编译 */
            const render = compileToFunction(template);
        }
    }
}