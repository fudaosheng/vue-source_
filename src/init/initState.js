import {observe} from './observe'
/**初始化状态
 * @vm指vue
 */
export function initState(vm) {
 
    const options = vm.$options;
    /**处理props、methods、data、computed、watch等 */
    if (options.props) {
        // console.log('props');
    }
    if (options.methods) {
        // console.log('methods');
    }
    if (options.data) {
        initData(vm);
    }
    if (options.computed) {
        // console.log('computed');
    }
    if (options.watch) {
        // console.log('watch')
    }
}

function initProps() { }
function initMethods() { }
function initData(vm) {

    let data = vm.$options.data;
    /**判断data类型，使拿到的data始终是{} 
     * 1.如果data是function，调用call()，call改变this的指向。
     * @例：
     * A.call(B,x,y)
      `改变函数A的this指向，使之指向B;
       把A函数放到B中运行(也可说成调用B中的A函数)，x和y是A函数的参数。
     * data.call(vm)--->vm.data();
     * 2.如果data是对象，直接使用
    */
    data = vm._data = typeof data === 'function' ? data.call(vm) : data;

    /**数据劫持 */
    observe(data);
}
function initComputed() { }
function initWatch() { }