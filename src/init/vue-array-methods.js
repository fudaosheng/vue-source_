/**改变vue data中数组的原型链，对Array部分方法进行拦截 */

const oldArrayMethods = Array.prototype;

/** 
 * vueArrayMethods.__proto__= Array.prototype
 * 
 * vueArrayMethods的原型链指向原生Array
*/
export const vueArrayMethods = Object.create(oldArrayMethods);

/**需要对vue中数组进行拦截的有：
 * pop、shift、unshift、push、splice、sort、reverse
 */

 const methods=[
     'pop',
     'shift',
     'push',
     'unshift',
     'push',
     'splice',
     'sort',
     'reverse'
 ]

 methods.forEach(method=>{
     /**重写Array方法
      * 对methods的方法拦截，然后调用原生Array相对于方法
      */
     vueArrayMethods[method]= function(...args){
         /**此处this指具体的data */
         const ob = this.__ob__;

         let inserted;//用户插入的元素
         /**调用原来Array的方法 */
         const result = oldArrayMethods[method].apply(this,args);

         /**几个方法特殊处理 */
         switch(method){
             case 'push':
             case 'unshift':
                 inserted=args;
                 break;
             case 'splice':
                 inserted=args.slice(2);//splice(index,decout,inserted)
                 break;
         }

         /**如果要插入的参数还是个数组，还要进行观察,即observeArray */
         if(inserted)ob.observeArray(inserted);

         return result;
     }
 })