(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, global.Vue = factory());
}(this, (function () { 'use strict';

  function _typeof(obj) {
    "@babel/helpers - typeof";

    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof = function (obj) {
        return typeof obj;
      };
    } else {
      _typeof = function (obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
      };
    }

    return _typeof(obj);
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  /**vue工具函数 */

  /**判断一个数据是不是对象类型，且不为空 */
  function isObject(data) {
    return _typeof(data) === 'object' && data !== null;
  }
  /**给一个对象精确的添加或修改属性,即对Object.defineProperty()进一步封装 */

  function def(obj, key, value) {
    Object.defineProperty(obj, key, {
      configurable: true,
      enumerable: false,
      //false不可枚举，不会被映射到for、Object.keys等中
      writable: true,
      //值是否可改变,
      value: value
    });
  }
  var unicodeRegExp = /a-zA-Z\u00B7\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u037D\u037F-\u1FFF\u200C-\u200D\u203F-\u2040\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD/;

  /**改变vue data中数组的原型链，对Array部分方法进行拦截 */
  var oldArrayMethods = Array.prototype;
  /** 
   * vueArrayMethods.__proto__= Array.prototype
   * 
   * vueArrayMethods的原型链指向原生Array
  */

  var vueArrayMethods = Object.create(oldArrayMethods);
  /**需要对vue中数组进行拦截的有：
   * pop、shift、unshift、push、splice、sort、reverse
   */

  var methods = ['pop', 'shift', 'push', 'unshift', 'push', 'splice', 'sort', 'reverse'];
  methods.forEach(function (method) {
    /**重写Array方法
     * 对methods的方法拦截，然后调用原生Array相对于方法
     */
    vueArrayMethods[method] = function () {
      /**此处this指具体的data */
      var ob = this.__ob__;
      var inserted; //用户插入的元素

      /**调用原来Array的方法 */

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      var result = oldArrayMethods[method].apply(this, args);
      /**几个方法特殊处理 */

      switch (method) {
        case 'push':
        case 'unshift':
          inserted = args;
          break;

        case 'splice':
          inserted = args.slice(2); //splice(index,decout,inserted)

          break;
      }
      /**如果要插入的参数还是个数组，还要进行观察,即observeArray */


      if (inserted) ob.observeArray(inserted);
      return result;
    };
  });

  /**对vue中data进行数据劫持 */

  function observe(data) {
    /**data不是对象直接返回 */
    if (!isObject(data)) return;
    /**响应式类 */

    new Observe(data);
  }
  /**处理data里面的每一个数据项 */

  var Observe = /*#__PURE__*/function () {
    function Observe(data) {
      _classCallCheck(this, Observe);

      /**对于data里面的数据，如果不是数组时执行一步处理walk
       * 是数组是，数组里面不是对象的不用监控，对象监控,并且对Array的一些方法进行拦截
       */

      /**给data中所有数据添加一个__ob__属性，该属性为Observe这个实例 */
      def(data, '__ob__', this);

      if (Array.isArray(data)) {
        /**
         * 对数组进行单独处理，对数组里面的普通项不进行观察，只有当数组里面项为对象时才进行监听
         * 重写Array，对数组方法进行拦截
         */
        data.__proto__ = vueArrayMethods;
        this.observeArray(data);
      } else {
        this.walk(data);
      }
    }
    /**一步处理data里面的数据
     * 一步处理时只处理对象
     */


    _createClass(Observe, [{
      key: "walk",
      value: function walk(data) {
        var keys = Object.keys(data);
        keys.forEach(function (key) {
          var value = data[key];
          /**核心-响应式处理
           * 用defineProperty对data里面的属性进行观察
           */

          defineReactive(data, key, value);
        });
      }
      /**Array响应式处理 */

    }, {
      key: "observeArray",
      value: function observeArray(data) {
        for (var i = 0, length = data.length; i < length; i++) {
          observe(data[i]);
        }
      }
    }]);

    return Observe;
  }();
  /**响应式处理data里面的数据 */


  function defineReactive(data, key, value) {
    /**如果data里面数据项也是一个Object，需要递归处理 */
    observe(value);
    Object.defineProperty(data, key, {
      get: function get() {
        return value;
      },
      set: function set(newValue) {
        if (value === newValue) return;
        console.log('data of set');
        /**当用户给data里面的数据赋值时，要将新赋的值也做响应式处理 */

        observe(newValue);
        value = newValue;
      }
    });
  }

  /**初始化状态
   * @vm指vue
   */

  function initState(vm) {
    var options = vm.$options;
    /**处理props、methods、data、computed、watch等 */

    if (options.props) ;

    if (options.methods) ;

    if (options.data) {
      initData(vm);
    }

    if (options.computed) ;

    if (options.watch) ;
  }

  function initData(vm) {
    var data = vm.$options.data;
    /**判断data类型，使拿到的data始终是{} 
     * 1.如果data是function，调用call()，call改变this的指向。
     * @例：
     * A.call(B,x,y)
      `改变函数A的this指向，使之指向B;
       把A函数放到B中运行(也可说成调用B中的A函数)，x和y是A函数的参数。
     * data.call(vm)--->vm.data();
     * 2.如果data是对象，直接使用
    */

    data = vm._data = typeof data === 'function' ? data.call(vm) : data;
    /**数据劫持 */

    observe(data);
  }

  var ncname = "[a-zA-Z_][\\-\\.0-9_a-zA-Z".concat(unicodeRegExp.source, "]*"); //匹配命名空间，abc-aaa 

  var qnameCapture = "((?:".concat(ncname, "\\:)?").concat(ncname, ")"); //匹配自定义标签：<aaa:asdas></aaa:asdas>

  var startTagOpen = new RegExp("^<".concat(qnameCapture)); //匹配开始标签,返回一个数组，0是"<abc:asd",1是"abc:asd"

  var endTag = new RegExp("^<\\/".concat(qnameCapture, "[^>]*>")); //匹配结束标签，如:</div>

  var attribute = /^\s*([^\s"'<>\/=]+)(?:\s*(=)\s*(?:"([^"]*)"+|'([^']*)'+|([^\s"'=<>`]+)))?/; //匹配属性

  var startTagClose = /^\s*(\/?)>/; //匹配标签结束>
  /**将template模板转换成render函数
   * ast语法树：用树的数据结构来描述原生html，虚拟dom：用对象描述节点 
   **/

  function compileToFunction(template) {
    /**将html转换成ast语法树 */
    var root = parseHTML(template);
    return function render() {};
  }
  /**格式化html字符串
   * 格式化步骤：逐步匹配，将匹配到的字符串删除。
   */

  function parseHTML(html) {

    while (html) {
      var textEnd = html.indexOf('<');
      /**1.匹配开始标签。textEnd=0，肯定是一个标签，要么开始标签要么结束标签 */

      if (textEnd == 0) {
        var startTagMatch = parseStartTag(); //匹配开始标签

        if (startTagMatch) {
          continue; //如果匹配到进行下一次匹配
        }
        /**匹配结尾 */


        var endTagMatch = html.match(endTag);

        if (endTagMatch) {
          advance(endTagMatch[0].length); //删除匹配到的结束标签

          continue; //进行下次匹配
        }
      }

      var text = void 0;
      /**2.匹配文本。即换行后的空白文本*/

      if (textEnd >= 0) {
        text = html.substring(0, textEnd);

        if (text) {
          advance(text.length);
        }
      }
      /**3.匹配结尾 */

    }
    /**格式化开始标签 */


    function parseStartTag() {
      /**1.匹配开始标签 */
      var start = html.match(startTagOpen); //start是一个数组，arguments[0]是<div ，arguments[1]是div

      var match = {
        tagName: [],
        attrs: [],
        type: 1,
        parent: null,
        children: []
      };
      /**如果匹配到开始标签 */

      if (start) {
        advance(start[0].length); //删除开始标签

        match.tagName.push(start[1]);
      }
      /**2.匹配属性 */


      var end, attr;
      /**attr格式是一个数组，arguments数组元素为：
       * 0: " id="app""
       *1: "id"
       *2: "="
       *3: "app"
       *4: undefined
       *5: undefined
       */

      /**当没有匹配到>结束字符，且属性不为空时执行循环 */

      while (!(end = html.match(startTagClose)) && (attr = html.match(attribute))) {
        /**删除属性 */
        advance(attr[0].length); //删除属性

        /**将提取到的属性添加到属性数组 */

        match.attrs.push({
          name: attr[1],
          value: attr[3] || attr[4] || attr[5]
        });
      } //3.匹配闭合字符>，去掉开始标签的>


      if (end) {
        advance(end.length); //去掉闭合字符>

        return match;
      }
    }
    /**advance：前进 */


    function advance(n) {
      html = html.slice(n);
    }
  }
  /**原生html转ast语法树示例： */

  /**
  <div id="app">
      <p>hello</p>
  </div>

   let root = {
      tag:'div',
      attrs:[
          {name:'id',value:'app'}
      ],
      type:1,
      parent:null,
      children:[
          {
              tag:'p',
              attrs:[],
              type:1,
              parent:root,
              children:[{
                  text:'hello',
                  type:3
              }]
          }
      ],
  }
   */

  /**vue初始化入口 */

  function initMixin(vue) {
    /**options new Vue时传进来的选项如props、data、computed、methods、watch等 */
    vue.prototype._init = function (options) {
      var vm = this; //vm就是vue

      /**将new Vue时传入的参数添加到Vue的$options属性上
       * this.$options即指的是用户传递的参数
       */

      vm.$options = options;
      /**初始化状态 */

      initState(vm);
      /**页面挂载与渲染 */

      if (vm.$options.el) {
        this.$mount(vm.$options.el);
      }
    };

    vue.prototype.$mount = function (el) {
      var vm = this;
      var options = vm.$options;
      /**获取el的dom元素 */

      el = document.querySelector(el);
      /**页面渲染顺序:render函数、template模板、html节点 */
      //没有render函数

      if (!options.render) {
        var template = options.template;
        /**没有render函数，并且没有template、有el模板 */

        if (!options.template && options.el) {
          template = el.outerHTML;
        }

        console.log(template);
        /**模板编译 */

        var render = compileToFunction(template);
      }
    };
  }

  /**vue声明、入口 */

  function Vue(options) {
    /**实际调用初始化方法 */
    this._init(options);
  }

  initMixin(Vue);

  return Vue;

})));
//# sourceMappingURL=vue.js.map
